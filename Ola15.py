import machine                                              #import library machine              
import utime                                                #import libraty utime

led1 = machine.Pin(14, machine.Pin.OUT)                     #store settings: pin14 to output into variable led1 
led2 = machine.Pin(15, machine.Pin.OUT)                     #store settings: pin15 to output into variable led2 
btn1 = machine.Pin(18, machine.Pin.IN, machine.Pin.PULL_UP) #store settings: pin18 to input.If not pressed value=1, into variable btn1
btn2 = machine.Pin(19, machine.Pin.IN, machine.Pin.PULL_UP) #store settings: pin19 to input.If not pressed value=1, into variable btn2 
led_onboard = machine.Pin(25, machine.Pin.OUT)              #store settings: pin25 in-built led to output into variable led_onboard

def led_1_blink():                                          #Def new function 
    led1.value(1)                                           #set led1 value to 1 
    utime.sleep(0.1)                                         #wait 0.1sec
    led1.value(0)                                           #set led1 value to 0

def led_onboard_blink():                                    #Def new function 
    led_onboard.value(1)                                    #set led_onboard value to 1
    utime.sleep(0.1)                                        #wait 0.1sec
    led_onboard.value(0)                                    #set led_onboard value to 0

def led_2_blink():                                          #Def new function
    led2.value(1)                                           #set led2 value to 1
    utime.sleep(0.1)                                        #wait 0.1sec
    led2.value(0)                                           #set led2 value to 0

def led_onboard_blink_twice():                              #Def new function
    for i in range(2):                                      #for loop. Repeat 2 times
        led_onboard.value(1)                                #set led_onboard value to 1
        utime.sleep(0.1)                                    #wait 0.1sec
        led_onboard.value(0)                                #set led_onboard value to 0
        utime.sleep(0.1)

while True:                                                 #while loop. Repeat to infinity
    if btn1.value() == 0:                                   #If statment
        led_1_blink()                                       #Do this If statement is true
        led_onboard_blink()                                 #Do this If statment is true 
    if btn2.value() == 0:                                   #If statment
        led_2_blink()                                       #Do this If statment is true 
        led_onboard_blink_twice()                           #Do this If statment is true